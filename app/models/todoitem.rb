class Todoitem < ApplicationRecord
	belongs_to :user
    validates :description, presence: true
    validates :status, inclusion: { in: %w(done created),
    message: "status only accept done or created" }

    validate :date_cannot_be_in_the_past,
    :due_time_cannot_earlier_than_the_date
 
	  def date_cannot_be_in_the_past
	    if date.present? && date < Date.today
	      errors.add(:date, "can't be in the past")
	    end
	  end
	 
	  def due_time_cannot_earlier_than_the_date
	    if due_time < date
	      errors.add(:due_time, "should no earlier than the date")
	    end
	  end

end
