require 'rails_helper'

RSpec.describe "todoitems/show", type: :view do
  before(:each) do
    @todoitem = assign(:todoitem, Todoitem.create!(
      :description => "MyText",
      :status => "Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Status/)
  end
end
