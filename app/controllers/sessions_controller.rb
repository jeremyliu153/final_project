class SessionsController < ApplicationController
  def new
  end
  def create
    user = User.find_by_email(params[:email])
    if !user
      flash.now[:alert] = "Email is invalid"
      render "new"
    elsif user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "Logged in!"
    elsif !user.authenticate(params[:password])
      flash.now[:alert] = "Password is invalid"
      render "new"
    else
      
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Logged out!"
  end
end