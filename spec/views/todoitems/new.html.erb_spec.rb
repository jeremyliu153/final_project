require 'rails_helper'

RSpec.describe "todoitems/new", type: :view do
  before(:each) do
    assign(:todoitem, Todoitem.new(
      :description => "MyText",
      :status => "MyString"
    ))
  end

  it "renders new todoitem form" do
    render

    assert_select "form[action=?][method=?]", todoitems_path, "post" do

      assert_select "textarea[name=?]", "todoitem[description]"

      assert_select "input[name=?]", "todoitem[status]"
    end
  end
end
