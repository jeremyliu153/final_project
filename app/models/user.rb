class User < ApplicationRecord
	has_many :todoitems
  has_secure_password
  validates :email, presence: true, uniqueness: true
  validates :password, format: { with: /\A^(?=.*?[a-z])(?=.*?[0-9]).{6,}$\z/,
    message: "no shorter than 6 character’s, password containing at least one number, one lowercase letter" }
end
