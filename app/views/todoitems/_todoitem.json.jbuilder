json.extract! todoitem, :id, :description, :date, :due_time, :status, :created_at, :updated_at
json.url todoitem_url(todoitem, format: :json)
