require 'rails_helper'

RSpec.describe "todoitems/index", type: :view do
  before(:each) do
    assign(:todoitems, [
      Todoitem.create!(
        :description => "MyText",
        :status => "Status"
      ),
      Todoitem.create!(
        :description => "MyText",
        :status => "Status"
      )
    ])
  end

  it "renders a list of todoitems" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
