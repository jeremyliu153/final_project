require 'rails_helper'

RSpec.describe "todoitems/edit", type: :view do
  before(:each) do
    @todoitem = assign(:todoitem, Todoitem.create!(
      :description => "MyText",
      :status => "MyString"
    ))
  end

  it "renders the edit todoitem form" do
    render

    assert_select "form[action=?][method=?]", todoitem_path(@todoitem), "post" do

      assert_select "textarea[name=?]", "todoitem[description]"

      assert_select "input[name=?]", "todoitem[status]"
    end
  end
end
