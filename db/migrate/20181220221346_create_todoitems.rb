class CreateTodoitems < ActiveRecord::Migration[5.2]
  def change
    create_table :todoitems do |t|
      t.text :description
      t.date :date
      t.date :due_time
      t.string :status
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
